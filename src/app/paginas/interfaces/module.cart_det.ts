export interface ModuleCartDet {
    cart_det_id?:       number;

    cart_cab_id?:       number;
    product_id:        number;
    cart_det_amount:   number;
    cart_det_price:    number;
    cart_det_subtotal: number;
}
